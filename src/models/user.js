import mongoose from 'mongoose'

const RefUserSchema = new mongoose.Schema({
  profile_id: {
      type: Number,
      required: true
    },
  profile_id_str: {
    type: String,
    required: true
  },  
  username: {
      type: String,
      required: true
    },
  name: {
      type: String,
      required: true
    },
  access_token: {
      type: String,
      required: true
    },
  secret_token: {
      type: String,
      required: true
    },
  picture: String,
});

const UserSchema = new mongoose.Schema({
    profile_id: {
        type: Number,
        required: true
      },
      profile_id_str: {
        type: String,
        required: true
      },
    username: {
        type: String,
        required: true
      },
    name: {
        type: String,
        required: true
      },
    access_token: {
        type: String,
        required: true
      },
    secret_token: {
        type: String,
        required: true
      },
    picture: String,
    type_account: {
      type: Number,
      default: 0
    },
    ref_users: [RefUserSchema],
  });
  
  export default mongoose.model('users', UserSchema)