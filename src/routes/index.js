import express from 'express';
import auth from '../controllers/auth';
import passport from 'passport';

const router = express.Router()

/* Login with Passport and Twitter Oauth */
router.route('/twitter/login').get(passport.authenticate('twitter'))
router.route('/twitter/return').get(passport.authenticate('twitter', { failureRedirect: '/twitter/login' }),
  function(req, res) {
    // Successful authentication, redirect success url.
    res.redirect('/success');
  });
router.route('/success').get(auth.success)

/* Braintree */
router.route('/client_token').get(auth.clientToken)
router.route('/checkout').post(auth.checkout)

/* Multiple Account */
router.route('/accounts').get(auth.accounts)
router.route('/add_account').get(auth.addAccount)
router.route('/delete_ref_user/:user_id/:ref_user_id').delete(auth.deleteRefUser)

/* Twitter API */
router.route('/show_user/:user_id').get(auth.showUser)
router.route('/favorites').get(auth.favoriteList)
router.route('/tweets_hashtag').get(auth.tweetsHashtag)
router.route('/tweets_location').get(auth.tweetsLocation)
router.route('/followers').get(auth.followers)
router.route('/friends').get(auth.friends)
router.route('/post_status').get(auth.postStatus)
router.route('/update_profile').post(auth.updateProfile)
router.route('/blocked_users').get(auth.blockedUsers)

/* Twitter ADS API */
router.route('/ads_accounts').get(auth.adsAccounts)
router.route('/analytics').get(auth.analytics)
router.route('/schedule_tweets').get(auth.getScheduledTweets)
router.route('/schedule_tweet').post(auth.postScheduledTweet)

export default router;