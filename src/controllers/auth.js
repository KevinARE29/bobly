import dotenv from 'dotenv'
import Twitter from 'twitter';
import getBearerToken from 'get-twitter-bearer-token';
import TwitterAdsAPI from 'twitter-ads';
import User from '../models/user';
import braintree from 'braintree';
var request = require('request').defaults({ encoding: null });
dotenv.config()
const consumer_key = process.env.CONSUMER_KEY;
const consumer_secret = process.env.CONSUMER_SECRET;

/* GET Bearer Token */
function bearerToken(){
    let PBearer = new Promise(function(res, rej){
        getBearerToken(consumer_key, consumer_secret, (err, twres) => {
            if(err) rej(err); //Handle with catch
            res(twres.body.access_token);
        })
    });
    return PBearer;
}

/* Upload an Image for use with Twitter API */
function uploadImage(T, url){
    let media_id = new Promise(function(res, rej){
        request.get(url, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                let data64 = Buffer.from(body).toString('base64');
                //console.log(data64);
                T.post('media/upload.json', {media_data: data64}, function(err, data, resp) {
                    if(err) rej(err); //Handle with catch
                    console.log(data.media_id_string);
                    res(data.media_id_string);
                });
            }
        });
    });
    return media_id;
}

/* GET Client Token for Braintree */
function clientToken(req, res, next) {
    var gateway = braintree.connect({
        environment: braintree.Environment.Sandbox,
        merchantId: process.env.MERCHANT_ID,
        publicKey: process.env.PUBLIC_KEY,
        privateKey: process.env.PRIVATE_KEY
    });
    gateway.clientToken.generate({}, function (err, response) {
        if (err) {
            throw new Error(err);
        }
        if (response.success) {
            return res.status(200).json(response.clientToken);
        } else {
            return res.status(500).json({msg: 'Something wrong'});
        } 
    });
}

function checkout(req, res, next) {
    var gateway = braintree.connect({
        environment: braintree.Environment.Sandbox,
        merchantId: process.env.MERCHANT_ID,
        publicKey: process.env.PUBLIC_KEY,
        privateKey: process.env.PRIVATE_KEY
    });
    //var nonceFromTheClient = req.body.payment_method_nonce;
    gateway.transaction.sale({
        amount: "40.00",
        //paymentMethodNonce: nonceFromTheClient,
        paymentMethodNonce: "fake-paypal-future-nonce",
        options: {
          submitForSettlement: true
        }
      }, function (err, result) {
          console.log(err);
          console.log(result);
          return res.status(200).json(result);
        // if (result.success) {
        //     // See result.transaction for details
        //     return res.status(200).json(result); 
        //   } else {
        //     // Handle errors
        //     console.log(err);
        //     return res.status(500).json(err); 
        //   }
      });
}

/* Login Success */
function success(req, res, next){
    return res.status(200).json(req.user);
}

/* Multiple Account */
function accounts(req, res, next){
    User.findOne({ 'profile_id' : req.body.profile_id }, function(err, user) {
        if (user) return res.status(200).json(user);
        else return res.status(404).json({message: "User not found!"});
    });
}

function addAccount(req, res, next){
    req.session.profile_id=req.body.profile_id;
    res.redirect('/twitter/login');
}

function deleteRefUser(req, res, next){
    let user_id = req.params.user_id;
    let ref_user_id = req.params.ref_user_id;
    
    User.findOne({_id: user_id}).then((user) => {
    if(!user) return Promise.reject({message: "Main account not found!", status: 404});

    user.ref_users.id(ref_user_id).remove();
    return user.save();
  }).then((user) => {
    res.status(200).json({msg: "Linked account deleted!"});
  }).catch(next);
}

/* TWITTER API */
async function favoriteList(req, res, next){
    const bearer_token = await bearerToken();

    var client = new Twitter({
        consumer_key: consumer_key,
        consumer_secret: consumer_secret,
        bearer_token: bearer_token
      });
    
    // Get Favorites
    client.get('favorites/list.json', {user_id: '763206925697839105', include_user_entities: false}, function(error, favorites, response) {
        let resp = [];
        let locations=['0015ae8c13de7bfd'];
        let words=['love', 'hello', 'Prueba'];
        favorites.forEach((favorite) => {
            // Filter by List of Locations
            if (favorite.place != null){
                if (locations.indexOf(favorite.place.id) > -1){
                    // resp.push(favorite)
                    // console.log(favorite.place)
                }
            }
            // Filter by Words
            if (words.some(function(v) { return favorite.text.toLowerCase().indexOf(v.toLowerCase()) >= 0; })) {
                resp.push(favorite)
                //console.log(favorite.text);
            }
        });
        return res.status(200).json(resp);
    });

}

/* Get Tweets Based on Hashtag */ 
async function tweetsHashtag(req, res, next){
    const bearer_token = await bearerToken();
    var client = new Twitter({
        consumer_key: consumer_key,
        consumer_secret: consumer_secret,
        bearer_token: bearer_token
    });
    client.get('search/tweets', {q: `#${req.body.hashtag}`}, function(error, tweets, response) {
        return res.status(200).json(tweets);
     });
}

/* Get Tweets Based on Location */ 
async function tweetsLocation(req, res, next){
    const bearer_token = await bearerToken();
    var client = new Twitter({
        consumer_key: consumer_key,
        consumer_secret: consumer_secret,
        bearer_token: bearer_token
    });
    client.get('search/tweets', {q: `place:${req.body.location_id}` }, function(error, tweets, response) {
        return res.status(200).json(tweets);
     });
}

/* Get Followers */ 
async function followers(req, res, next){
    const bearer_token = await bearerToken();
    var client = new Twitter({
        consumer_key: consumer_key,
        consumer_secret: consumer_secret,
        bearer_token: bearer_token
    });

    client.get('followers/list.json', {user_id: req.body.user_id, skip_status: true, include_user_entities: false}, function(error, followers, response) {
       return res.status(200).json(followers);
    });
}

/* Get Friends */ 
async function friends(req, res, next){
    const bearer_token = await bearerToken();
    var client = new Twitter({
        consumer_key: consumer_key,
        consumer_secret: consumer_secret,
        bearer_token: bearer_token
    });

    client.get('friends/list.json', {user_id: req.body.user_id, skip_status: true, include_user_entities: false}, function(error, friends, response) {
       return res.status(200).json(friends);
    });
}

/* Show User  */ 
async function showUser(req, res, next){
    const bearer_token = await bearerToken();
    var client = new Twitter({
        consumer_key: consumer_key,
        consumer_secret: consumer_secret,
        bearer_token: bearer_token
    });
    client.get('users/show', {user_id: req.params.user_id}, function(error, user, response) {
        console.log(user.followers_count);
        return res.status(200).json(user);
    });
}

function postStatus(req, res, next){
    var client = new Twitter({
        consumer_key: consumer_key,
        consumer_secret: consumer_secret,
        access_token_key: '763206925697839105-ewAJ8KEhI37vUcMcTQ0rOeGjJihQcQ5',
        access_token_secret: '1F50iQcNmUoic26ySSla99qc3t6KhzPmHrlb1vjfFBsDn'
    });
    
    /* ACTUALIZAR ESTADO */
    //client.post('statuses/update', {status: 'I Love Twitter'},  function(error, tweet, response) {
    //    if(error) throw error;
    //    console.log(tweet);  // Tweet body.
    //    console.log(response);  // Raw response object.
    //    return res.status(200).json(tweet);
    //  });

    /* ENVIAR DIRECT MESSAGE */
    // client.post('direct_messages/new.json', {
    //     user_id: '763206925697839105',    
    //     text: 'Hello World!',    
    //   },  function(error, response) {
    //         if(error) console.log(response);
    //         console.log(response);  // Raw response object.status
    //         return res.status(200).json(response);
    //       });

    /* OBTENER LISTA DE USUARIOS SILENCIADOS */
    // client.get('mutes/users/list.json', function(error, response) {
    //         if(error) console.log(response);
    //         console.log(response);  // Raw response object.status
    //         return res.status(200).json(response);
    //       });

    /* SILENCIAR UN USUARIO ESPECIFICO */
    // client.post('mutes/users/create.json', {
    //     screen_name: 'AlexCon77122597',
    //     // user_id: '',  
    //   },  function(error, response) {
    //         if(error) console.log(response);
    //         console.log(response);  // Raw response object.status
    //         return res.status(200).json(response);
    //       });

    /* DESILENCIAR UN USUARIO ESPECIFICO */
    // client.post('mutes/users/destroy.json', {
    //     screen_name: 'AlexCon77122597',
    //     // user_id: '',  
    //   },  function(error, response) {
    //         if(error) console.log(response);
    //         console.log(response);  // Raw response object.status
    //         return res.status(200).json(response);
    //       });

    

    /* BLOQUEAR UN USUARIO ESPECIFICOrouter.route('/tweets_hashtag').get(auth.tweetsHashtag) */
    // client.post('blocks/create.json', {
    //     screen_name: 'AlexCon77122597',
    //     // user_id: '',          
    //   },  function(error, response) {
    //         if(error) console.log(response);
    //         console.log(response);  // Raw response object.status
    //         return res.status(200).json(response);
    //       });

    /* DESBLOQUEAR UN USUARIO ESPECIFICO */
    // client.post('blocks/destroy.json', {
    //     screen_name: 'AlexCon77122597',
    //     // user_id: '',       
    //   },  function(error, response) {
    //         if(error) console.log(response);
    //         console.log(response);  // Raw response object.status
    //         return res.status(200).json(response);
    //       });


    /* Geo Search */
    // client.get('geo/search.json', {query: 'Santa Tecla, El Salvador'}, function(error, response) {
    //         if(error) console.log(response);
    //         console.log(response);  // Raw response object.status
    //         return res.status(200).json(response);
    //       });

    /* FOLLOW */
    /* Follow User */
    //client.post('friendships/create.json', {screen_name: 'AlexCon77122597'}, function(error, response) {
    //    if(error) console.log(response);
    //    console.log(response);  // Raw response object.status
    //    return res.status(200).json(response);
    //});

    /* UnFollow User */
    //client.post('friendships/destroy.json', {screen_name: 'AlexCon77122597'}, function(error, response) {
    //    if(error) console.log(response);
    //    console.log(response);  // Raw response object.status
    //    return res.status(200).json(response);
    //});

    /* RETWEET */
    // Retweet a Tweet
    //client.post('statuses/retweet', {id: '509457288717819904', trim_user: true}, function(error, response) {
    //    if(error) console.log(response);
    //    console.log(response);  // Raw response object.status
    //    return res.status(200).json(response);
    //});

    // UnRetweet a Tweet
    //client.post('statuses/unretweet', {id: '509457288717819904', trim_user: true}, function(error, response) {
    //    if(error) console.log(response);
    //    console.log(response);  // Raw response object.status
    //    return res.status(200).json(response);
    //});
      
}

/* UPDATE PROFILE */
function updateProfile(req, res, next){
    var client = new Twitter({
        consumer_key: consumer_key,
        consumer_secret: consumer_secret,
        access_token_key: req.body.access_token,
        access_token_secret: req.body.access_token_secret,
    });

    client.post('account/update_profile', {name: req.body.name},  function(error, response) {
        if(error) console.log(error);
        return res.status(200).json(response);
    });
}

/* GET BLOCKED USERS LIST */
function blockedUsers(req, res, next){
    if(!req.user){
        return res.redirect('/twitter/login');
    }
    var client = new Twitter({
        consumer_key: consumer_key,
        consumer_secret: consumer_secret,
        access_token_key: req.user.access_token,
        access_token_secret: req.user.secret_token,
    });

    client.get('blocks/list.json', function(error, response) {
        if(error) console.log(response);
        console.log(response);  // Raw response object.status
        return res.status(200).json(response);
    });
}

/* TWITTER ADS API */
/* GET ADS ACCOUNTS */
function adsAccounts(req, res, next){
    var client = new TwitterAdsAPI({
        consumer_key: consumer_key,
        consumer_secret: consumer_secret,
        access_token: req.param.access_token,
        access_token_secret: req.params.access_token_secret,
        sandbox: false, // defaults to true
        api_version: '2' //defaults to 2
      });
    client.get('accounts', function(error, resp, body) {
    if (error) return console.error(error);
        return res.status(200).json(body);
    });
}

/* GET ANALYTICS */
function analytics(req, res, next){
    var client = new TwitterAdsAPI({
        consumer_key: consumer_key,
        consumer_secret: consumer_secret,
        access_token: req.body.access_token,
        access_token_secret: req.body.access_token_secret,
        sandbox: false, // defaults to true
        api_version: '2' //defaults to 2
      });
      
    client.get('stats/accounts/18ce54qn65g', {end_time: '2018-07-24T07:00:00Z', entity: 'ACCOUNT', granularity: 'TOTAL', metric_groups: 'ENGAGEMENT', placement: 'ALL_ON_TWITTER', start_time: '2018-07-18T07:00:00Z'}, function(error, resp, body) {
        if (error) return res.status(200).json(error);
        else  return res.status(200).json(body);
    });
}

/* GET SCHEDULED TWEETS */
function getScheduledTweets(req, res, next){
    var client = new TwitterAdsAPI({
        consumer_key: consumer_key,
        consumer_secret: consumer_secret,
        access_token: req.body.access_token,
        access_token_secret: req.body.access_token_secret,
        sandbox: false, // defaults to true
        api_version: '2' //defaults to 2
      });
      
    client.get('accounts/:account_id/scheduled_tweets', {account_id: req.body.account_id}, function(error, resp, body) {
        if (error) return console.error(error);
        return res.status(200).json(body);
    });
}

/* POST A SCHEDULED TWEET */
async function postScheduledTweet(req, res, next){
    var client = new TwitterAdsAPI({
        consumer_key: consumer_key,
        consumer_secret: consumer_secret,
        access_token: req.body.access_token,
        access_token_secret: req.body.access_token_secret,
        sandbox: false, // defaults to true
        api_version: '2' //defaults to 2
    });
    var T = new Twitter({
        consumer_key: consumer_key,
        consumer_secret: consumer_secret,
        access_token_key: req.body.access_token,
        access_token_secret: req.body.access_token_secret
    });
    
    if(req.body.media){
        let media_ids=[];
        
        for (const url of req.body.media) {
            media_ids.push(await uploadImage(T,url));
        }
        console.log(media_ids);
        client.post('accounts/:account_id/scheduled_tweets', 
            {account_id: req.body.account_id, scheduled_at: req.body.scheduled_at, text: req.body.text, 
            as_user_id: req.body.as_user_id, nullcast: false, media_ids: media_ids}, function(error, resp, body) {
            if (error) return console.error(error);
            return res.status(200).json(body);
        });
    } else {
        client.post('accounts/:account_id/scheduled_tweets', 
            {account_id: req.body.account_id, scheduled_at: req.body.scheduled_at, text: req.body.text, 
            as_user_id: req.body.as_user_id, nullcast: false}, function(error, resp, body) {
            if (error) return console.error(error);
            return res.status(200).json(body);
        });
    }    
}

export default { bearerToken, favoriteList, postStatus, getScheduledTweets, postScheduledTweet, adsAccounts, 
    tweetsHashtag, tweetsLocation, followers, friends, updateProfile, blockedUsers, success, addAccount, analytics, 
    accounts, deleteRefUser, clientToken, checkout, showUser }
