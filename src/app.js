import dotenv from 'dotenv';
dotenv.config();

import express from 'express';
import mongoose from 'mongoose'

import bodyParser from 'body-parser';
import cors from 'cors';
import helmet from 'helmet';

import morgan from 'morgan';

import routes from './routes';

import passport from 'passport';
var TwitterStrategy = require('passport-twitter').Strategy;
import session from 'express-session';

import User from './models/user';
const AutoDM = require("./controllers/AutoDM");

passport.use(new TwitterStrategy({
  consumerKey: `${process.env.CONSUMER_KEY}`,
  consumerSecret: `${process.env.CONSUMER_SECRET}`,
  callbackURL: `${process.env.CALLBACK_URL}`,
  passReqToCallback : true, // allows us to pass in the req from our route (lets us check if a user is logged in or not)
  state: true
}, 
function(req, token, tokenSecret, profile, done) {
  // asynchronous
  //console.log(profile);
  process.nextTick(function() {
      // check if the user is already logged in
      if (!req.user) {
        if (typeof req.session.profile_id !== 'undefined' & (parseInt(profile.id) != parseInt(req.session.profile_id))){
          // Add a Secundary Account
          console.log("IF");
          User.findOne({ 'profile_id' : req.session.profile_id }, function(err, user) {
            if (err)
                return done(err);
            let ref_users = user.ref_users;
            console.log(ref_users);
            const ref_user = ref_users.find( r_user => r_user.profile_id === parseInt(profile.id) );
            console.log(ref_user);
            if (ref_user) {
              console.log("2do if");          
                  return done(null, user);
            }else {
              console.log("Else");
              user.ref_users.push({profile_id: profile.id, profile_id_str: profile._json.id_str, access_token: token, 
                secret_token:tokenSecret, username: profile.username, name: profile.displayName, 
                picture: profile._json.profile_image_url_https.replace("_normal.", ".")});
              user.save(function(err) {
                if (err)
                    return done(err); 
                return done(null, user);
              });
            }
          });
        }
        else{
          User.findOne({ 'profile_id' : profile.id }, function(err, user) {
              if (err)
                  return done(err);
              if (user) {
                  // if there is a user id already but no token (user was linked at one point and then removed)
                  if (!user.access_token) {
                      user.access_token = token;
                      user.secret_token = tokenSecret;
                      user.username = profile.username;
                      user.name = profile.displayName;
                      user.picture = profile._json.profile_image_url_https.replace("_normal.", ".");
                      user.save(function(err) {
                          if (err)
                              return done(err);
                              
                          return done(null, user);
                      });
                  }
                  return done(null, user); // user found, return that user
              } else {
                  // if there is no user, create them
                User.findOne(
                    {'ref_users.profile_id': profile.id},
                    {ref_users:{$elemMatch:{profile_id: profile.id}}},function(err, secundary_account) {
                      if (secundary_account){
                        return done(null, secundary_account.ref_users);
                      }else{
                        var newUser                 = new User();
                        newUser.profile_id          = profile.id;
                        newUser.profile_id_str = profile._json.id_str;
                        newUser.access_token = token;
                        newUser.secret_token = tokenSecret;
                        newUser.username = profile.username;
                        newUser.name = profile.displayName;
                        newUser.picture = profile._json.profile_image_url_https.replace("_normal.", ".");
                        newUser.save(function(err) {
                            if (err)
                              return done(err);
                                
                          return done(null, newUser);
                        });    
                      }
                    });
              }
          });
        }
      } else {
          // user already exists and is logged in
          return done(null, req.user);
      }
  });
}));

passport.serializeUser(function(user, callback) {
  callback(null, user);
})

passport.deserializeUser(function(obj, callback) {
  callback(null, obj);
})

const app = express()

// *************************************************************
// ******************   Configs    *****************************
// *************************************************************

// Parse body params and attache them to req.body
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Secure apps by setting various HTTP headers
app.use(helmet());

// Enable CORS - Cross Origin Resource Sharing
app.use(cors())

// *************************************************************
// ******************   Passport    ****************************
// *************************************************************
app.use(session({secret: 'whatever', resave: true, saveUninitialized: true, }));
app.use(passport.initialize());
app.use(passport.session());

// *************************************************************
// ******************   Logs    ********************************
// *************************************************************

// Console logger
app.use(morgan('dev'));


// *************************************************************
// ******************   Server    ******************************
// *************************************************************

// Opening port
app.listen(process.env.PORT, ()=> {
  console.log("Server started on port", process.env.PORT + "!")
});

// Emulate the body of the methods POST and PUT, when it's a GET or delete
app.use( (req, res, next) => {
  // Allow Access control for other origins
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Authorization, Accept");
  res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS");
  if('OPTIONS' == req.method) res.send(200);
  else{
    if('GET' == req.method || "DELETE" == req.method) req.body = req.query;
    next();
  }
})

// Using routes from index.
app.use('/', routes);

// Error handle middleware
app.use((err, req, res, next) => {

  let msg;
  let status;
  let errorName;

  if(err.name == "CastError" && err.path.includes("id")){
    msg = "Invalid cast for ObjectId";
    status = 400;
    errorName = "CastError";
  }
  else if(err.name == "ValidationError"){
    msg = err.message;
    status = 400;
    errorName = "ValidationError";
  }
  else{
    msg = err.message || err;
    status = err.status || 500;
    errorName = err.name || "Error";
  }

  if(typeof msg == "Object") msg = JSON.stringify(msg);

  // HTTP Response
  res.status(status).json({'msg': msg, errorType: errorName })
});


// *************************************************************
// ******************   Mongo    *******************************
// *************************************************************
mongoose.connect(process.env.db, { keepAlive: true }, (err) => {
  if(err) console.error(`Unable to connect to database ${process.env.db}`);
  else console.log(`Database connected successfully!: ${process.env.db}`);
});

AutoDM();

export default app
